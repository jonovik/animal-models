
# This is the user-interface definition of a Shiny web application.
# You can find out more about building applications with Shiny here:
#
# http://shiny.rstudio.com
#

library(shiny)

source("global.R")

sliderInputs <- plyr::alply(inputs, 1, function(df) do.call(shiny::sliderInput, df[1,]))

shinyUI(fluidPage(

  # Application title
  titlePanel("A purposeful simplification of four-legged animals"),

  # Sidebar with a slider input for size of animal
  sidebarLayout(
    # Plot the animal
    mainPanel(
      plotOutput("animalPlot")
    ),
    sidebarPanel(
      c(list(actionButton("randomize", "Randomize!")), sliderInputs)
    )
  )
))
